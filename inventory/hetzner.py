#!/usr/bin/env python3

import json
import os
import sys
import requests

try:
    token = os.environ["HCLOUD_TOKEN"]
except KeyError:
    print('  Error: You need HCLOUD_TOKEN defined', file=sys.stderr)
else:
    headers = {"Authorization": f"Bearer {token}"}
    response = requests.get('https://api.hetzner.cloud/v1/servers',
                            headers = headers)
    api_output = json.loads(response.content)
    all_groups = {'children': {'hcloud': {'children': {}}}}
    label_groups = set()
    label_members = {}
    cluster_ips = {}
    hosts = []
    meta = {}
    for server in api_output['servers']:
        name = server['name']
        hosts.append(name)
        info = {}
        info["datacenter"] = server['datacenter']['name']
        info["id"] = server['id']
        info["image_id"] = server['image']['id']
        info["image_name"] = server['image']['name']
        info["image_os_flavor"] = server['image']['os_flavor']
        info["labels"] = server['labels']
        info["location"] = server['datacenter']['location']['name']
        info["name"] = server['name']
        info["network_zone"] = server['datacenter']['location']['network_zone']
        info["server_type"] = server['server_type']['name']
        info["status"] = server['status']
        info["type"] = server['server_type']['name']
        info["ipv4"] = ""
        if 'ipv4' in server['public_net'] and \
          server['public_net']['ipv4'] and \
          'ip' in server['public_net']['ipv4']:
            info["ipv4"] = server['public_net']['ipv4']['ip']
            info["ipv4_external"] = server['public_net']['ipv4']['ip']
            if len(server['private_net']) > 0 and \
              'ip' in server['private_net'][0]:
                info["ipv4_internal"] = server['private_net'][0]['ip']
        else:
            info["ipv4_internal"] = server['private_net'][0]['ip']
            info["ipv4"] = server['private_net'][0]['ip']
        for key, value in info["labels"].items():
            if value not in ("client", "server"):
                continue
            label = f"{key}_{value}"
            label_groups.add(label)
            if label in label_members:
                label_members[label].append(name)
            else:
                label_members[label] = [name]
            if value == 'server':
                if key in cluster_ips:
                    cluster_ips[key].append(info['ipv4_internal'])
                else:
                    cluster_ips[key] = [info['ipv4_internal']]
        meta[name] = info
    hcloud_groups = list(label_groups)
    all_groups['children']['hcloud'] = {'children': hcloud_groups}
    inventory = {"_meta": {"hostvars": meta},
                 "all": all_groups,
                 "hcloud": {"hosts": hosts}}
    for group in label_members.keys():
        cluster = group.split('_')[0]
        inventory[group] = {'hosts': label_members[group]}
        if cluster in cluster_ips:
            inventory[group]['vars'] = {f'{cluster}_cluster': cluster_ips[cluster]}
        if group in ('nomad_client', 'vault_server', 'loadbalancer_server'):
            inventory[group]['vars']['consul_cluster'] = cluster_ips['consul']
    print(json.dumps(inventory))
